from pykml import parser
from lxml import etree
import os

defects_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Siddarth/gpr/Barod/ERT/1'

project_path = "'http://183.82.33.43/2k20/HERO_Barod_GPR/images/ERT/"

# inverters =[]
#
# for i,j,k in os.walk(defects_path):
#     for files in j:
#         inverters.append(os.path.join(i,files))


# <html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>Panel Number</td><td> 5U</td></tr><tr><td><span style='font-weight:bold'>Defect Type</td><td> Hotspot</td></tr><tr><td><span style='font-weight:bold'>Criticality</td><td> Minor</td></tr><tr><td><span style='font-weight:bold'>Description</td></tr><tr><td colspan='2'><span style='font-weight:bold'> Stressed/Damaged cell. High reflection on a single celll</td></tr><tr><td></td><td></td></tr><tr><td><span style='font-weight:bold'>Latitude</td><td> 13.7417</td></tr><tr><td><span style='font-weight:bold'>Longitude</td><td> 78.6212</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= 'http://106.51.3.224:6660/2k19/hero_thermal/Cropped_Images/ITC_5/ITC_5_4.png'></td></tr></table></body></html>

# < tr class ='w3-green' > < tr > < td > < span style='font-weight:bold' > String Code < / td > < td > 5.1.1.4.2 < / td > < / tr >

def changer(kml_file,output_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document.Folder

    for place in doc.Placemark:
        temp = place.description
        latlon = str(place.Point.coordinates)
        latlon = latlon.split(',')
        temp = str(temp)
        Northing = latlon[1] + ' N'
        Easting = latlon[0]+ ' E'

        BH_No = temp.split("BH_No</B> = ")[1].split("<BR><B>Northing")[0]
        # Northing = temp.split("Northing</B> = ")[1].split("<BR><B>Easting")[0]
        # Easting = temp.split("Easting</B> = ")[1].split("<BR><B>image")[0]
        img_no = temp.split("<B>image</B> = ")[1].split("]]")[0]

        # b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>ITC No:</td><td>"+itc+"</td></tr><tr><td><span style='font-weight:bold'>Table No:</td><td>"+tbno+"</td></tr><tr><td><span style='font-weight:bold'>Defect:</td><td>"+defect+"</td></tr><tr><td><span style='font-weight:bold'>Description:</td><td>"+descrip+"</tr><tr><td><span style='font-weight:bold'>Latitude:</td><td>"+lat+"</td></tr><tr><td><span style='font-weight:bold'>Longitude:</td><td>"+long+"</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/images/"+ inv_name+'/'+defect+'/'+tbno+".jpg'></td></tr></table></body></html>"
        b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>BH_No:</td><td>" + BH_No + "</td></tr><tr><td><span style='font-weight:bold'>Latitude:</td><td>" + Northing + "</td></tr><tr><td><span style='font-weight:bold'>Longitude:</td><td>" + Easting + "</td></tr><tr><td><span style='font-weight:bold'>Image</td><td><img src= " + project_path + img_no + ".jpg'></td></tr></table></body></html>"

        # temp_1 = str(temp)
        place.description._setText(b)

    with open(output_file, "w") as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    with open(output_file, "ab") as f:
        f.write(etree.tostring(docs, pretty_print=True))



# for i in inverters:
#     from pathlib import Path
#
#     # inv_name = Path(i).name
#     # inv_name = inv_name.split('.')[0]
#
#     path=[]
#     for m,n,o in os.walk(i):
#         for file in o:
#             path.append(os.path.join(m,file))
#
#     for j in path:
#         changer(j,j,inv_name)




changer('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/GPR/Barod/ERT_BAROD (copy).kml',
        '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/GPR/Barod/ERT_BAROD (copy).kml',
        )