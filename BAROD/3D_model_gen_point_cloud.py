
# ========================== ENV is synegnta ================================================
import open3d as o3d
import numpy as np


source_data = {2.2: {'p1': [(0, 2.2), (10, 2.1), (15, 2.3), (21, 2.1)],
                     'p2': [(6, 2.3), (12, 2.1), (14, 2.0), (16, 2.2)],
                     'p3': [(0, 2.2), (13, 2.1), (17, 2.3), (21, 2.1)],
                     'p4': [(0, 2.2), (6, 2.3), (12, 2.1), (16, 2.0)]},
               5: {'p1': [(0, 5), (10, 4.8), (16, 4.9), (21, 5)],
                   'p2': [(0, 5), (4, 4.7), (11, 4.8), (16, 2.2)],
                   'p3': [(0, 5), (10, 4.8), (16, 4.9), (21, 5)],
                   'p4': [(0, 5), (4, 4.7), (11, 4.8), (16, 5)]}}

pages = list(source_data.keys())

final_points = []

for a in source_data.values():

    for (i, k) in a.items():
        temp = []
        for j in k:
            if i == 'p1':
                temp.append([j[0], 0, j[1]])
            elif i == 'p2':
                temp.append([21, j[0], j[1]])
            elif i == 'p3':
                temp.append([j[0], 21, j[1]])
            elif i == 'p4':
                temp.append([0, j[0], j[1]])

        a[i] = temp


    for i in a.values():
        for j in i:
            final_points.append(j)





final_points = np.array(final_points)

# pcl = o3d.geometry.PointCloud()
# pcl.points = o3d.utility.Vector3dVector(final_points)
# o3d.visualization.draw_geometries([pcl])
# 
# o3d.io.write_point_cloud("summa.ply", pcl)


# ========================== trainglation ==========================

final_points1 = final_points[:16]
final_points2 = final_points[16:]

print(len(final_points1))
print(len(final_points2))


x = np.array([i[0] for i in final_points1])
y = np.array([i[1] for i in final_points1])
z = np.array([i[2] for i in final_points1])

x1 = np.array([i[0] for i in final_points2])
y1 = np.array([i[1] for i in final_points2])
z1 = np.array([i[2] for i in final_points2])

from mayavi import mlab
mlab.figure(1, fgcolor=(0, 0, 0), bgcolor=(0, 0, 0))

# Visualize the points
pts = mlab.points3d(x, y, z, z, scale_mode='none', scale_factor=0.2)

pts1 = mlab.points3d(x1, y1, z1, z1, scale_mode='none', scale_factor=0.2)

# Create and visualize the mesh
mesh = mlab.pipeline.delaunay2d(pts)
surf = mlab.pipeline.surface(mesh)

mesh1 = mlab.pipeline.delaunay2d(pts1)
surf1 = mlab.pipeline.surface(mesh1)


mlab.view(47, 57, 8.2, (0.1, 0.15, 0.14))
mlab.show()