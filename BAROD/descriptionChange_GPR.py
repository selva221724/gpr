from pykml import parser
from lxml import etree
import os

defects_path = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Automation_Team/Siddharth/gpr/Barod/kml"

project_path = "'http://183.82.33.43/2k20/HERO_Barod_GPR/images/GPR"

kmls =[]

for i,j,k in os.walk(defects_path):
    for files in k:
        kmls.append(os.path.join(i,files))
        

def changer(kml_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document.Folder

    for place in doc.Placemark:
        temp = place.description
        temp = str(temp)
        UT_Zone = temp.split("UT_Zone</B> = ")[1].split("<BR><B>")[0]
        Imgno = temp.split("Image_name</B> = ")[1].split("]]>")[0]
        mapname = temp.split("MAP_NAME</B> = ")[1].split("<BR><B>Borehole_n")[0]
        if "," in Imgno:
            Imglist = Imgno.split(",")
            b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>UT_Zone</td><td>"+UT_Zone+"</td></tr><tr><td><span style='font-weight:bold'>Map Name</td><td>"+mapname+"</td></tr>"
            for i in Imglist:
                b += "<tr><td><span style='font-weight:bold'>"+i+"</td><td><img src= " + project_path+"/"+UT_Zone+'/'+i+".JPG'></td></tr>"
            b += "</table></body></html>" 
        else:
            b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>UT_Zone</td><td>"+UT_Zone+"</td></tr><tr><td><span style='font-weight:bold'>Map Name</td><td>"+mapname+"</td></tr><tr><td><span style='font-weight:bold'>Image no:</td><td>"+Imgno+"</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/"+UT_Zone+'/'+Imgno+".JPG'></td></tr></table></body></html>"
        
        place.description._setText(b)
        with open(kml_file, "w") as f:
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        with open(kml_file, "ab") as f:
            f.write(etree.tostring(docs, pretty_print=True))

for i in kmls:
    changer(i)