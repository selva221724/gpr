import cv2
import geo
import numpy as np
from osgeo import gdal
import pykml
import pyproj
from pyproj import Proj, transform
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path

kml_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/GPR/Barod/gpr_lines_for_arrow'

out = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/GPR/Barod/IT_Delivery/arrow'

tif = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/GPR/Merged_Ritis_AT_44MW_transparent_mosaic_group1.tif'


# coords,des = geo.kread(kml_path,description=True)
def kgen(features, KML_name, kml_type='polygon'):
    if kml_type == 'polygon':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newpolygon(outerboundaryis=[row[0], row[1], row[2], row[3], row[0]])
            # pol.description = des[i]
        kml.save(KML_name)

    elif kml_type == 'line':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newlinestring(coords=row)
            pol.linestyle.color = simplekml.Color.rgb(255, 0, 0)
            pol.linestyle.width = 8
        kml.save(KML_name)

    elif kml_type == 'point':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newpoint(coords=[row])
        kml.save(KML_name)

def w2p(x, y, geoTrans):
    x, y = geo.set_crs(x, y, '4326', '32643')
    x, y = geo.world2Pixel(geoTrans, x, y)
    return (x, y)

def p2w(x, y, geoTrans):

    x, y = geo.Pixel2world(geoTrans, x, y)
    x, y = geo.set_crs(x, y, '32643' ,'4326')
    return (x, y)


def read_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except Exception as e:
            print(e)
            return

    coords = []
    des = []
    for place in doc.Placemark:
        try:

            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        except:
            try:
                x = str(place.LineString.coordinates)
            except Exception as e:
                print(e)
                return

        y = str(place.description)
        des.append(y)

        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        v = x.split(",")
        try:
            i = 0
            coo = []
            while i < len(v):
                coo.append((v[i], v[i + 1]))
                i += 3
        except:
            pass

        coords.append(coo)

    print('number of Coordinates Extracted from KML is', len(coords))

    return coords, des


kmls =[]
for i,j,k in os.walk(kml_path):
    for file in k:
        kmls.append(os.path.join(i,file))

from natsort import natsorted

kmls = natsorted(kmls)

srcImage = gdal.Open(tif)
geoTrans = srcImage.GetGeoTransform()


def arrow(a):
    co, des = a['Coordinates'], a['Description']

    coo = []
    for i in co:
        x = [w2p(j[0], j[1], geoTrans) for j in i]
        coo.append(x)

    coo_selected = [i[0] for i in coo]

    from matplotlib import pyplot as plt

    f = sorted(coo_selected, key=lambda k: [k[1]])

    # iteration 2
    if f[0][0] > f[1][0]:
        x4, y4 = f[0]
        x2, y2 = f[1]
    else:
        x4, y4 = f[1]
        x2, y2 = f[0]
    if f[2][0] > f[3][0]:
        x3, y3 = f[2]
        x1, y1 = f[3]
    else:
        x3, y3 = f[3]
        x1, y1 = f[2]
    # x1,y1 = f[0]
    # x2,y2 = f[1]
    # x3,y3 = f[2]
    # x4,y4 = f[3]

    buffer = 30

    arraow_coords = [[[x1, y1], [x1 - buffer, y1 - buffer]], [[x1, y1], [x1 + buffer, y1 - buffer]],
                     [[x2, y2], [x2 + buffer, y2 + buffer]], [[x2, y2], [x2 + buffer, y2 - buffer]],
                     [[x3, y3], [x3 - buffer, y3 - buffer]], [[x3, y3], [x3 - buffer, y3 + buffer]],
                     [[x4, y4], [x4 + buffer, y4 + buffer]], [[x4, y4], [x4 - buffer, y4 + buffer]]]

    # plt.scatter(x, y)

    ac = []
    for i in arraow_coords:
        x = [p2w(j[0], j[1], geoTrans) for j in i]
        ac.append(x)

    return ac


def arro(kml,kml_name):

    coords, des = read_kml(kml)

    print(des)
    
    line_groups = []
    
    for i in des:
        poly_number = i.split("Map Name</td><td>")[1].split("</td></tr><tr><td><span style=")[0]
        if poly_number not in line_groups:
            line_groups.append(poly_number)
    
    source_dict = {}
    for i in line_groups:
    
        temp_coord = []
        temp_des = []
        for j in range(len(coords)):
            poly_number = des[j].split("Map Name</td><td>")[1].split("</td></tr><tr><td><span style=")[0]
            if poly_number == i:
                temp_coord.append(coords[j])
                temp_des.append(des[j])
    
        source_dict.update({i: {'Coordinates': temp_coord, 'Description': temp_des}})
    
    
    src_list = list(source_dict.values())
    

    # arrow_coords=[]
    # arrow_coords.append(arrow(src_list))
    
    arrow_coords=[]
    for i in src_list:
        arrow_coords.append(arrow(i))
    
    
    final_coords =[]
    for i in arrow_coords:
        for j in i:
            final_coords.append(j)
    
    
    
    kgen(final_coords,kml_name,'line')

count = 0
for i in kmls:
    print(str(count+1))

    arro(i,out+'/'+str(count+1)+'.kml')

    count+=1