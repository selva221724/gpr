from pykml import parser
from lxml import etree
import os

defects_path = "/mnt/dash/Alpha_Share/GPR/Badhla/IT_Delivery/GLOBAL/GPR_Lines/1"

project_path = "'http://183.82.33.43/2k20/HERO_Badhla_GPR/images/GPR"

kmls = []

for i, j, k in os.walk(defects_path):
    for files in k:
        kmls.append(os.path.join(i, files))


def changer(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document

    for place in doc.Placemark:
        temp = place.description
        temp = str(temp)
        UT_Zone = temp.split("Borehole_n</B> = ")[1].split("<BR><B>EAST")[0]
        Imgno = temp.split("<B>Id</B> = ")[1].split("<BR><B>")[0]
        UT_Zone_path = UT_Zone.replace(' ','')
        double = ['1', '3']
        single = ['2', '4']

        b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>BH_NO:</td><td>" + UT_Zone + "</td></tr>"
        if Imgno in single:
            b += "<tr><td><span style='font-weight:bold'>" + Imgno + "</td><td><img src= " + project_path + "/" + UT_Zone_path + '/Process Data/' + Imgno + ".JPG'></td></tr>"

        elif Imgno in double:
            b += "<tr><td><span style='font-weight:bold'>" + Imgno + "</td><td><img src= " + project_path + "/" + UT_Zone_path + '/Process Data/' + Imgno + ".JPG'></td></tr><tr><td><span style='font-weight:bold'>" + Imgno+'_1' + "</td><td><img src= " + project_path + "/" + UT_Zone_path + '/Process Data/' + Imgno+"_1"+ ".JPG'></td></tr>"


        # if "," in Imgno:
        #     Imglist = Imgno.split(",")
        #     b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>BH_NO:</td><td>" + UT_Zone + "</td></tr>"
        #     for i in Imglist:
        #         b += "<tr><td><span style='font-weight:bold'>" + i + "</td><td><img src= " + project_path + "/" + UT_Zone + '/Process Data/' + i + ".JPG'></td></tr>"
        #     b += "</table></body></html>"
        # else:
        #     b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>BH_NO:</td><td>" + UT_Zone + "</td></tr><tr><td><span style='font-weight:bold'>Image no:</td><td>" + Imgno + "</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/" + UT_Zone + '/Process Data/' + Imgno + ".JPG'></td></tr></table></body></html>"

        place.description._setText(b)
        with open(kml_file, "w") as f:
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        with open(kml_file, "ab") as f:
            f.write(etree.tostring(docs, pretty_print=True))


for i in kmls:
    changer(i)
