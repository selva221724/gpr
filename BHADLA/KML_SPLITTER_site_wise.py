import cv2
from geo import *
import numpy as np
from osgeo import gdal
import pykml
import pyproj
from pyproj import Proj, transform
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path

kml_path = '/mnt/dash/Alpha_Share/GPR/Badhla/IT_Delivery/Bhadhla.kml'
out_path = '/mnt/dash/Alpha_Share/GPR/Badhla/IT_Delivery/GPR_Lines'

def read_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except Exception as e:
            print(e)
            return

    coords = []
    des = []
    for place in doc.Placemark:
        try:

            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        except:
            try:
                x = str(place.LineString.coordinates)
            except Exception as e:
                print(e)
                return

        y = str(place.description)
        des.append(y)

        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        v = x.split(",")
        try:
            i = 0
            coo = []
            while i < len(v):
                coo.append((float(v[i]), float(v[i + 1])))
                i += 3
        except:
            pass

        coords.append(coo)

    print('number of Coordinates Extracted from KML is', len(coords))

    return coords, des


def kgen(features, KML_name, kml_type='polygon', description=None):
    if kml_type == 'polygon':
        kml = simplekml.Kml()
        for row,des in zip(features,description):
            pol = kml.newpolygon(outerboundaryis=[row[0], row[1], row[2], row[3], row[0]])
            if description:
                pol.description = des
        kml.save(KML_name)

    elif kml_type == 'line':
        kml = simplekml.Kml()
        for row,des in zip(features,description):
            pol = kml.newlinestring(coords=row)

            pol.linestyle.color = simplekml.Color.rgb(0,255,0)
            pol.linestyle.width = 5

            if description:
                pol.description = des
        kml.save(KML_name)

    elif kml_type == 'point':
        kml = simplekml.Kml()
        for row,des in zip(features,description):
            pol = kml.newpoint(coords=[row])
            if description:
                pol.description = des

            pol.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pol.style.labelstyle.scale = 2  # Make the text twice as big

            pol.style.iconstyle.icon.href = 'http://183.82.33.43/GroupL/colour_icon/blue.png'
        kml.save(KML_name)

# ================================ Point KML =====================================================

#
# coords, des = kread(kml_path, kml_type='point', description=True)
#
# coords_new = [i[:-1] for i in coords]
#
# c = list(zip(coords_new,des))
#
# c.sort(key=lambda x: x[0], reverse=False)
#
# coords_new , des = zip(*c)
#
# for i in range(len(coords_new)):
#     kgen([coords_new[i]], out_path+'/'+str(i + 1) + '.kml', kml_type='point', description=[des[i]])


# ================================ Line KML =====================================================

coords, des = read_kml(kml_path)


line_groups = []

for i in des:
    try:
        poly_number = i.split("MAP_NAME</B> = ")[1].split(".kml<BR><B>Borehole_n</B>")[0]
    except:
        poly_number = 'r2'
    if poly_number not in line_groups:
        line_groups.append(poly_number)


source_dict = {}
for i in line_groups:

    temp_coord = []
    temp_des = []
    for j in range(len(coords)):
        try:
            poly_number = des[j].split("MAP_NAME</B> = ")[1].split(".kml<BR><B>Borehole_n</B>")[0]
        except:
            poly_number = 'r2'
        if poly_number == i:
            temp_coord.append(coords[j])
            temp_des.append(des[j])

    source_dict.update({i: {'Coordinates': temp_coord, 'Description': temp_des}})

sorted_x = sorted(source_dict.items(), key=lambda kv: kv[1]['Coordinates'][0][0])
sorted_x = dict(sorted_x)


count =0
for i,j in sorted_x.items():

    coords_new = j['Coordinates']

    des = j['Description']
    c = list(zip(coords_new,des))

    c.sort(key=lambda x: x[0], reverse=False)

    coords_new , des = zip(*c)


    kgen(coords_new, out_path+'/'+str(count + 1) + '.kml', kml_type='line', description=des)

    count+=1